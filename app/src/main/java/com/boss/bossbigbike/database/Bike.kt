package com.boss.bossbigbike.database
import android.graphics.Bitmap
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter

@Entity(tableName = "bike_table")
data class Bike(
    @PrimaryKey(autoGenerate = true)
    var bike_Id: Long = 0L,

    @ColumnInfo(name = "bike_gen")
    var bike_gen:String? = null,

    @ColumnInfo(name = "bike_year")
    var bike_year:String? = null,

    @ColumnInfo(name = "bike_detail")
    var bike_detail:String? = null,

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    var bike_image: ByteArray? = null,

    @ColumnInfo(name = "brand_name")
    var brand_name:String? = null
)
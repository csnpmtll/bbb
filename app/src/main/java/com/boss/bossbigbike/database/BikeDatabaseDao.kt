package com.boss.bossbigbike.database
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface BikeDatabaseDao {
    @Insert
    fun insertBike(bike: Bike)

    @Query("SELECT * FROM bike_table ORDER BY bike_Id DESC")
    fun getAllBike(): LiveData<List<Bike>>

    @Query("DELETE FROM bike_table WHERE bike_gen = :key")
    fun clear(key: String)

    @Query("SELECT * FROM bike_table WHERE brand_name = :key")
    fun getBikeFromBrand(key: String): LiveData<List<Bike>>

    @Query("SELECT * FROM bike_table ORDER BY bike_Id DESC")
    fun getBike(): Bike?
}
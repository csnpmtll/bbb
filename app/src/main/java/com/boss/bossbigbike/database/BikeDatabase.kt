package com.boss.bossbigbike.database
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.icu.lang.UCharacter.GraphemeClusterBreak.V




@Database(entities = [Bike::class], version = 2, exportSchema = false)
abstract class BikeDatabase : RoomDatabase(){
    abstract val bikeDatabaseDao: BikeDatabaseDao

    companion object {

        @Volatile
        private var INSTANCE: BikeDatabase? = null

        fun getInstance(context: Context): BikeDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        BikeDatabase::class.java,
                        "bike_database"
                    ).fallbackToDestructiveMigration().build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}
package com.boss.bossbigbike.addBike

import android.app.Application
import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.ViewModel
import com.boss.bossbigbike.database.Bike
import com.boss.bossbigbike.database.BikeDatabaseDao
import com.boss.bossbigbike.getBytes
import kotlinx.coroutines.*


class AddBikeViewModel(
    dataSource: BikeDatabaseDao,
    application: Application
) : ViewModel() {
    val database = dataSource
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val bike = database.getAllBike()
    fun onAdd(
        img: Bitmap,
        gen: String,
        year: String,
        detail: String,
        brand: String
    ) {
        uiScope.launch {
            val bike = Bike()
            bike.bike_gen = gen
            bike.bike_year = year
            bike.bike_detail = detail
            bike.bike_image = getBytes(img)
            bike.brand_name = brand
            insert(bike)
            Log.i("AddBike","Bike Added")
        }
    }
    private suspend fun insert(bike: Bike) {
        withContext(Dispatchers.IO) {
            database.insertBike(bike)
        }
    }
    override fun onCleared() {
        super.onCleared()
        Log.i("AddBikeViewModel", "AddBikeViewModel destroyed!")
    }
}
package com.boss.bossbigbike.addBike
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import android.widget.Toast
import androidx.core.graphics.drawable.toBitmap
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.boss.bossbigbike.R
import com.boss.bossbigbike.database.BikeDatabase
import com.boss.bossbigbike.databinding.AddbikeFragmentBinding

class AddBikeFragment : Fragment() {
    private lateinit var addBikeViewModel: AddBikeViewModel
    private lateinit var binding:AddbikeFragmentBinding
    val CAMERA_REQUEST_CODE=0
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.addbike_fragment, container, false)
        val application = requireNotNull(this.activity).application
        val dataSource = BikeDatabase.getInstance(application).bikeDatabaseDao
        addBikeViewModel = ViewModelProviders.of(this,AddBikeViewModelFactory(
            dataSource, application)).get(AddBikeViewModel::class.java)
        binding.imageButton.setOnClickListener {
            val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if(callCameraIntent.resolveActivity(this.activity!!.packageManager)!=null){
                startActivityForResult(callCameraIntent,CAMERA_REQUEST_CODE)
            }
        }
        binding.addBtn.setOnClickListener{
            addBikeViewModel.onAdd(
                binding.imageButton.drawable.toBitmap(),
                binding.gen.text.toString(),
                binding.year.text.toString(),
                binding.detail.text.toString(),
                binding.spinner.selectedItem.toString())
            findNavController().navigate(AddBikeFragmentDirections.actionNavigationAddbikeToNavigationHome())
            Toast.makeText(this.requireContext(),"Added",Toast.LENGTH_SHORT).show()
        }
        return binding.root
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            CAMERA_REQUEST_CODE ->{
                if(resultCode== Activity.RESULT_OK && data !=null){
                    binding.imageButton.setImageBitmap(data.extras?.get("data") as Bitmap)
                }
            }
            else -> {
                Toast.makeText(this.requireContext(),"Unrecognized request code",Toast.LENGTH_SHORT)
            }
        }
    }
}
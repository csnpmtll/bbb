package com.boss.bossbigbike.addBike

import android.app.Application
import android.widget.ImageView
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.boss.bossbigbike.database.BikeDatabaseDao

class AddBikeViewModelFactory(
    private val dataSource: BikeDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddBikeViewModel::class.java)) {
            return AddBikeViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
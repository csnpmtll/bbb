package com.boss.bossbigbike.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.boss.bossbigbike.R
import com.boss.bossbigbike.database.BrandList
class HomeViewModel : ViewModel() {
    lateinit var brandList: MutableList<BrandList>
    fun brandList(){
        brandList = mutableListOf(
            BrandList("bmw", R.drawable.bmw)
            ,BrandList("aprilia", R.drawable.aprilia)
            ,BrandList("gpx", R.drawable.gpx)
            ,BrandList("ducati", R.drawable.ducati)
            ,BrandList("harleydavidson", R.drawable.harleydavidson)
            ,BrandList("honda", R.drawable.honda)
            ,BrandList("husqvarna", R.drawable.husqvarna)
            ,BrandList("kawasaki", R.drawable.kawasaki)
            ,BrandList("ktm", R.drawable.ktm)
            ,BrandList("motorguzzi", R.drawable.motorguzzi)
            ,BrandList("royalenfield", R.drawable.royalenfield)
            ,BrandList("suzuki", R.drawable.suzuki)
            ,BrandList("yamaha", R.drawable.yamaha)
            ,BrandList("triumph", R.drawable.triumph))
    }
    init {
        brandList()
        Log.i("HomeViewModel", "HomeViewModel created!")
    }
    override fun onCleared() {
        super.onCleared()
        Log.i("HomeViewModel", "HomeViewModel destroyed!")
    }
}
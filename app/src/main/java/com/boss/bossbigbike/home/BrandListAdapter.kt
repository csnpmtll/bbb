package com.boss.bossbigbike.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.boss.bossbigbike.R
import com.boss.bossbigbike.database.BrandList
import kotlinx.android.synthetic.main.custom_brandlist.view.*

class BrandListAdapter(val items: MutableList<BrandList>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_brandlist, parent, false))
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.brandImage.setImageResource(items.get(position).brand_image)
        holder.brandImage.setOnClickListener {
            it.findNavController().navigate(HomeFragmentDirections
                .actionHomeFragmentToCategoryFragment(items.get(position).brand_name,items.get(position).brand_image)
            )
        }
    }
    override fun getItemCount(): Int {
        return items.size
    }
}
class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val brandImage = view.brandImage
}
package com.boss.bossbigbike.category

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.boss.bossbigbike.database.BikeDatabaseDao

class CategoryViewmodelFactory(
    val brandName:String,
    val dataSource: BikeDatabaseDao,
    private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CategoryViewModel::class.java)) {
            return CategoryViewModel(brandName,dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

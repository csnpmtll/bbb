package com.boss.bossbigbike.category
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.graphics.drawable.toIcon
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.boss.bossbigbike.R
import com.boss.bossbigbike.database.Bike
import com.boss.bossbigbike.category.CategoryFragmentDirections
import com.boss.bossbigbike.getImage
import kotlinx.android.synthetic.main.custom_categorylist.view.*
import java.nio.IntBuffer

class CategoryListAdapter:RecyclerView.Adapter<CategoryListAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val bike_gen: Button = itemView.findViewById(R.id.bike_genBtn)
        fun bind(item: Bike, position: Int) {
            bike_gen.text = item.bike_gen.toString()
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int): ViewHolder {
        return from(parent)
    }

    var data = listOf<Bike>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        val res = holder.itemView.context.resources
        holder.bike_gen.text = item.bike_gen.toString()
        holder.bike_gen.setOnClickListener {
            it.findNavController().navigate(
                CategoryFragmentDirections
                    .actionNavigationCategoryToBikeDetailFragment(
                        item.bike_gen.toString(),
                        item.bike_year.toString(),
                        item.bike_detail.toString(),
                        getImage(item.bike_image!!),
                        item.brand_name.toString()
                    )
            )
        }
    }
    companion object {
        private fun from(parent: ViewGroup): ViewHolder {
            val layoutInflater =
                LayoutInflater.from(parent.context)
            val view = layoutInflater
                .inflate(R.layout.custom_categorylist,
                    parent, false)
            return ViewHolder(view)
        }
    }

}



package com.boss.bossbigbike.category

import android.annotation.SuppressLint
import android.app.ActionBar
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.boss.bossbigbike.R
import com.boss.bossbigbike.database.Bike
import com.boss.bossbigbike.database.BikeDatabase
import com.boss.bossbigbike.databinding.CategoryFragmentBinding
import com.boss.bossbigbike.getBytes
import com.boss.bossbigbike.getImage


class CategoryFragment: Fragment() {
    private lateinit var categoryViewModel: CategoryViewModel
    private lateinit var binding: CategoryFragmentBinding
    val adapter = CategoryListAdapter()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.category_fragment, container, false)
        var actionBar: ActionBar? = activity!!.actionBar

        val application = requireNotNull(this.activity).application
        val dataSource = BikeDatabase.getInstance(application).bikeDatabaseDao
        categoryViewModel = ViewModelProviders.of(this,CategoryViewmodelFactory(
            CategoryFragmentArgs.fromBundle(arguments!!).brandName,
            dataSource, application)).get(CategoryViewModel::class.java)
        binding.setLifecycleOwner(this)

        categoryViewModel.bike.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.data = it
                Log.i("bike",it.toString())
            }
        })
        categoryViewModel.navigateToBikeDetail.observe(this, Observer { bike ->
            bike?.let {
                this.findNavController().navigate(
                    CategoryFragmentDirections
                        .actionNavigationCategoryToBikeDetailFragment(bike.bike_gen.toString(),bike.bike_year.toString(),bike.bike_detail.toString(),
                            getImage(bike.bike_image!!),bike.brand_name.toString()))
                categoryViewModel.doneNavigating()
            }
        })
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.categoryList.adapter = adapter
        binding.categoryList.layoutManager = LinearLayoutManager(context)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.category, categoryViewModel.brandName)
    }
}
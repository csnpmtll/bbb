package com.boss.bossbigbike.category

import android.app.Application
import android.provider.SyncStateContract.Helpers.insert
import android.util.Log
import androidx.lifecycle.*
import com.boss.bossbigbike.R
import com.boss.bossbigbike.database.Bike
import com.boss.bossbigbike.database.BikeDatabaseDao
import kotlinx.coroutines.*

//import com.boss.bossbigbike.database.BikeDatabaseDao

class CategoryViewModel(
    val brandName: String,
    val database: BikeDatabaseDao,
    application: Application) : AndroidViewModel(application) {

    var bike = database.getBikeFromBrand(brandName)
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private var bikelist = MutableLiveData<Bike?>()

    private val _navigateToBikeDetail = MutableLiveData<Bike>()
    val navigateToBikeDetail: LiveData<Bike>
        get() = _navigateToBikeDetail

    init {
        initializeBike()
        Log.i("brandName", brandName)
    }
    fun doneNavigating() {
        _navigateToBikeDetail.value = null
    }
    private fun initializeBike() {
        uiScope.launch {
            bikelist.value = getBikeFromDatabase()
        }
    }
    private suspend fun getBikeFromDatabase(): Bike? {
        return withContext(Dispatchers.IO) {
            var bike = database.getBike()
            bike
        }
    }
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
        Log.i("CategoryViewModel", "CategoryViewModel destroyed!")
    }
}
package com.boss.bossbigbike.bikedetail
import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.boss.bossbigbike.database.BikeDatabaseDao

class BikeDetailViewModelFactory(private val bike_gen: String,
                                 private val bike_year: String,
                                 private val bike_detail: String,
                                 private val bike_image: Bitmap,
                                 private val brand_name: String,
                                 private val dataSource: BikeDatabaseDao,
                                 private val application: Application
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BikeDetailViewModel::class.java)) {
            return BikeDetailViewModel(bike_gen,bike_year,bike_detail,bike_image,brand_name,dataSource,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
package com.boss.bossbigbike.bikedetail

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.ContextWrapper
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.boss.bossbigbike.R
import com.boss.bossbigbike.database.BikeDatabase
import com.boss.bossbigbike.databinding.BikedetailFragmentBinding
import com.google.android.material.snackbar.Snackbar
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment.DIRECTORY_PICTURES
import android.os.Environment
import android.util.Log
import androidx.core.view.drawToBitmap
import java.io.*
import java.util.*


class BikeDetailFragment : Fragment() {
    private lateinit var viewModel: BikeDetailViewModel
    private lateinit var binding:BikedetailFragmentBinding
    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.bikedetail_fragment, container, false)
        val application = requireNotNull(this.activity).application
        val dataSource = BikeDatabase.getInstance(application).bikeDatabaseDao
        viewModel = ViewModelProviders.of(this,BikeDetailViewModelFactory(
            BikeDetailFragmentArgs.fromBundle(arguments!!).bikeGen,
            BikeDetailFragmentArgs.fromBundle(arguments!!).bikeYear,
            BikeDetailFragmentArgs.fromBundle(arguments!!).bikeDetail,
            BikeDetailFragmentArgs.fromBundle(arguments!!).bikeImage,
            BikeDetailFragmentArgs.fromBundle(arguments!!).brandName,
            dataSource,application)).get(BikeDetailViewModel::class.java)
        viewModel.bike_gen.observe(this, Observer { newBikeGen ->
            binding.bikeGen.text = newBikeGen.toString()
        })
        viewModel.bike_year.observe(this, Observer { newBikeYear ->
            binding.bikeYear.text = newBikeYear.toString()
        })
        viewModel.bike_detail.observe(this, Observer { newBikeDetail ->
            binding.bikeDetail.text = newBikeDetail.toString()
        })
        viewModel.bike_image.observe(this, Observer { newBikeImage ->
            binding.bikeImage.setImageBitmap(newBikeImage)
        })
        viewModel.brand_name.observe(this, Observer { newBrandName ->
            binding.bikeBrand.text = newBrandName.toString()
        })
        binding.bikeDetailViewModel = viewModel
        binding.deleteBtn.setOnClickListener {
            viewModel.onDelete()
            findNavController().navigate(BikeDetailFragmentDirections.actionBikeDetailFragmentToNavigationHome())
            Snackbar.make(this.requireView(),"DELETED",Snackbar.LENGTH_SHORT).show()
        }
        binding.lifecycleOwner = this
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.bike_detail, viewModel.bike_gen.value.toString())
        return binding.root
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.share_menu, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.shareMenuButton -> onShare()
        }
        return super.onOptionsItemSelected(item)
    }
    private fun onShare() {
        val shareIntent = ShareCompat.IntentBuilder.from(Activity())
            .setText(viewModel.bike_gen.value.toString()+"\n"+
                    viewModel.bike_year.value.toString()+"\n"+
                    viewModel.bike_detail.value.toString()+"\n"+
                    viewModel.brand_name.value.toString())
            .setType("text/plain")
            .intent;
        try {
            startActivity(shareIntent)
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(requireContext(), "Share not available",Toast.LENGTH_LONG).show()
        }
    }
}
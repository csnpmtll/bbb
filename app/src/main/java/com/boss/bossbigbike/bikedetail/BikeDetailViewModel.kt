package com.boss.bossbigbike.bikedetail

import android.app.Application
import android.graphics.Bitmap
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.boss.bossbigbike.category.CategoryFragmentDirections
import com.boss.bossbigbike.database.BikeDatabaseDao
import kotlinx.coroutines.*

class BikeDetailViewModel(
    bikeGen: String,
    bikeYear: String,
    bikeDetail: String,
    bikeImage: Bitmap,
    brandName: String,
    val database: BikeDatabaseDao,
    application: Application
) : ViewModel() {
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val _bike_gen = MutableLiveData<String>()
    val bike_gen: LiveData<String>
        get() = _bike_gen

    private val _bike_year = MutableLiveData<String>()
    val bike_year: LiveData<String>
        get() = _bike_year

    private val _bike_detail = MutableLiveData<String>()
    val bike_detail: LiveData<String>
        get() = _bike_detail

    private val _bike_image = MutableLiveData<Bitmap>()
    val bike_image: LiveData<Bitmap>
        get() = _bike_image

    private val _brand_name = MutableLiveData<String>()
    val brand_name: LiveData<String>
        get() = _brand_name
    init {
        _bike_gen.value = bikeGen
        _bike_year.value = bikeYear
        _bike_detail.value = bikeDetail
        _bike_image.value = bikeImage
        _brand_name.value = brandName
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("BikeDetailViewModel", "BikeDetailViewModel destroyed!")
    }
    fun onDelete() {
        uiScope.launch {
            clear()
        }
    }
    suspend fun clear() {
        withContext(Dispatchers.IO) {
            database.clear(_bike_gen.value!!)
        }
    }
}
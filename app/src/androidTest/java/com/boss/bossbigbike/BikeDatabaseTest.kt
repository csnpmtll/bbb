package com.boss. bossbigbike

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.boss.bossbigbike.database.Bike
import com.boss.bossbigbike.database.BikeDatabase
import com.boss.bossbigbike.database.BikeDatabaseDao
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import android.content.ContentValues
import android.graphics.Bitmap
import android.R
import android.R.attr.*
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import junit.framework.Assert.assertTrue
import org.junit.Assert
import kotlin.math.log


@RunWith(AndroidJUnit4::class)
class BikeDatabaseTest {
    private lateinit var bikeDao: BikeDatabaseDao
    private lateinit var db: BikeDatabase
    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        // Using an in-memory database because the information stored here disappears when the
        // process is killed.
        db = Room.inMemoryDatabaseBuilder(context, BikeDatabase::class.java)
            // Allowing main thread queries, just for testing.
            .allowMainThreadQueries()
            .build()
        bikeDao = db.bikeDatabaseDao
        Log.i("Database","Connected to database")
    }
    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }
    @Test
    @Throws(Exception::class)
    fun insertAndGetBike() {
        val bitmap = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.menu_frame)
        val image = getBytes(bitmap)
        val bike = Bike(0,
            "bmw1000CC",
            "1997",
            "This is BMW",
            ByteArray(R.drawable.bottom_bar) ,
            "bmw")
        bikeDao.insertBike(bike)
        val b = bikeDao.getBikeFromBrand("bmw")
        Assert.assertNotNull(b)
    }
}